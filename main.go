package main

import (
	"fmt"
	"log"
	"os"
	"gorm.io/gorm"
	"gorm.io/driver/postgres"
)

/* https://www.quora.com/What-do-you-call-a-person-who-goes-to-the-library-Es-shop-customer
People who use the library are generally referred to as "patrons." Their purpose in visiting does not matter -- whether to peruse the books, listen to audio recordings, read newspapers, or to enjoy special events.
*/
type Patron struct {
	// Inserts SQL fieldsin 1 line of code (This will be a SQL object)
	gorm.Model

	Name	string
	Email	string	`gorm:"typevarchar(100);unique_index"` //unique_index makes sure there's only one
	Books	[]Book

}

type Book struct {
	gorm.Model

	Title string
	Author string
	ISBN	int
	// Gorm requires ID to be placed after the name (name of struct, something after it)
	PatronID	int
}

// Test data that we'll put in the database
var (
	patron = &Patron{
		Name: "Jack",
		Email: "jack@example.com",
	}
	books = []Book{
		{Title: "The Rules of Thinking", Author: "Richard Templar", ISBN: 9781292263816, PatronID: 1},
		{Title: "The Total Money Makeover: A Proven Plan for Financial Fitness", Author: "Dave Ramsey", ISBN: 9780785289081, PatronID: 1},

	}
)

/* Global Variables
*/
var db *gorm.DB // Initialize DB client
var err error

 
func main() {
	/* Information to gain access to the database
	- You don't want to store sensitive information in the code: Use environment variables instead
	*/
	sqlHost := os.Getenv("DB_HOST")
	sqlPort := os.Getenv("DB_PORT")
	sqlUser := os.Getenv("DB_USER")
	sqlPassword := os.Getenv("DB_PASSWORD")
	sqlDBName := os.Getenv("DB_NAME")
	// Database connection string
	fmt.Println(fmt.Sprintf("host=%s user=%s dbname=%s password=%s port=%s", sqlHost, sqlUser, sqlDBName, sqlPassword, sqlPort))

	sqlURI := fmt.Sprintf("host=%s user=%s dbname=%s password=%s port=%s", sqlHost, sqlUser, sqlDBName, sqlPassword, sqlPort)

	// Open the DB connection
	db, err = gorm.Open(postgres.Open(sqlURI), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("SQL Connect Success")
	}

	// If database not in use, and app not running, close database (GORM v1)
	// defer db.Close()

	/* Do database migrations (Put the structs in the Database)
	Automatically migrate your schema, to keep your schema up to date.
	*/
	db.AutoMigrate(&Patron{})
	db.AutoMigrate(&Book{})

	// Put the person in the database
	db.Create(&patron)

	// Put the books in the database
	for idx := range books {
		db.Create(&books[idx])
	}
	
}